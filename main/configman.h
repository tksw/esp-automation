#ifndef __HOMEAUTO_CONFIGMAN_
#define __HOMEAUTO_CONFIGMAN_

#include <stdint.h>
#include <stddef.h>

void *configman_get_value(const char *key, const char *value);

// will use nvs filesystem as backend
// eventually will also have a hashtable cache that will keep track of writes
// and will periodically writeback to filesystem
// temporarily a singly linked list as its not priority rn
typedef struct {
        char *key;
        char *value;
} config_item;

/* typedef struct { */
/*         // Size of items is unknown at comptime so we use malloc */

/* } config_store; */

typedef struct config_node {
        config_item *data;
        struct config_node *next;
} config_node;

config_item CONFIG_EMPTY_ITEM = { NULL, NULL };

config_node CONFIG_EMPTY_NODE = { &CONFIG_EMPTY_ITEM, NULL };

config_node *config_head = &CONFIG_EMPTY_NODE;

#endif

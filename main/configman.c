#include "configman.h"

#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

config_item *configman_new_item(const char *key, const char *value)
{
        config_item *temp = malloc(sizeof(config_item));
        temp->key = strdup(key);
        temp->value = strdup(key);
        return temp;
}

void configman_del_item(config_item *item_to_be_popped)
{
        if (item_to_be_popped == &CONFIG_EMPTY_ITEM)
                return;

        free(item_to_be_popped->key);
        free(item_to_be_popped->value);
        free(item_to_be_popped);
}

void configman_pop(const char *key)
{
        config_node *current = config_head;
        config_node *prev;

        // walk till we reach same key
        while (strcmp(current->data->key, key) != 0 &&
               current != &CONFIG_EMPTY_NODE) {
                prev = current;
                current = current->next;
        }

        configman_del_item(current->data);
        prev->next = current->next;
        free(current);
}

void configman_append(const char *key, const char *value)
{
        config_node *curr = config_head;

        config_node *to_be_added = malloc(sizeof(config_node));
        if (!to_be_added)
                return;

        to_be_added->data = configman_new_item(key, value);

        while (curr->next)
                curr = curr->next;

        if (curr == config_head)
                config_head = configman_new_item(key, value);
}
